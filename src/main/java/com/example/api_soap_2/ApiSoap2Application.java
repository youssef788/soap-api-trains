package com.example.api_soap_2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiSoap2Application {

    public static void main(String[] args) {
        SpringApplication.run(ApiSoap2Application.class, args);
    }

}
