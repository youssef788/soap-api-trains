package com.example.api_soap_2.service;

import fr.model.reservation.Reservation;
import fr.model.train.Train;
import fr.model.user.CreateReservationRequest;
import fr.model.user.FilterAvailableTrainsRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class TrainService {

    public List<Train> getAllTrain(FilterAvailableTrainsRequest request){
        RestTemplate restTemplate = new RestTemplate();
        if(request == null){

            Train[] trainsRequests = restTemplate.getForObject("http://localhost:8081/allTrains", Train[].class);

            return trainsRequests == null ? Collections.emptyList() : Arrays.asList(trainsRequests);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<FilterAvailableTrainsRequest> httpEntity = new HttpEntity<>(request, headers);

        Train[] trainsRequests = restTemplate.postForObject("http://localhost:8081/availableTrains", httpEntity, Train[].class);

        return trainsRequests == null ? Collections.emptyList() : Arrays.asList(trainsRequests);

    }

    public Reservation createReservation(CreateReservationRequest request){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<CreateReservationRequest> httpEntity = new HttpEntity<>(request, headers);
        try {
            return restTemplate.postForObject("http://localhost:8081/createReservation", httpEntity, Reservation.class);
        }catch (RestClientException e){
            return null;
        }
    }


}
