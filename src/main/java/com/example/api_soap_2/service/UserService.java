package com.example.api_soap_2.service;

import com.example.api_soap_2.dao.UserRepository;
import fr.model.user.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class UserService {
    private UserRepository userRepository = new UserRepository();
    public static User session = null;

    public UserService(){
        session = null;
    }

    public User login(String userName, String password) {
        if(session == null) {
            for (User tmp : userRepository.findAllUsers()) {
                if (Objects.equals(tmp.getUserName(), userName)
                        && Objects.equals(tmp.getPassword(), password)) {
                    session = tmp;
                    return tmp;
                }
            }
        }
        return null;
    }
    public void logout() {
        session = null;
    }
    public List<User> getAllUsers(){
        return userRepository.findAllUsers();
    }
}
