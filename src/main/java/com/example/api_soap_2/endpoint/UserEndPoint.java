package com.example.api_soap_2.endpoint;

import com.example.api_soap_2.service.TrainService;
import com.example.api_soap_2.service.UserService;
import fr.model.reservation.Reservation;
import fr.model.user.GetAllTrainsResponse;
import fr.model.train.Train;
import fr.model.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.io.IOException;
import java.util.List;

@Endpoint
public class UserEndPoint {
    private final UserService userService;
    private final TrainService trainService;

    private static final String NAMESPACE_URI = "http://model.fr/user";

    private final String CONTENT_TYPE_ACCEPT = "Accept";
    private final String CONTENT_TYPE_APPJSON = "application/json";
    private final String CONTENT_TYPE = "Content-Type";
    private final String CONTENT_TYPE_APPJSON_UTF8 = CONTENT_TYPE_APPJSON + "; charset=UTF-8";

    @Autowired
    public UserEndPoint(TrainService trainService) {
        this.trainService = trainService;
        this.userService = new UserService();
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllUsersRequest")
    @ResponsePayload
    public GetAllUsersResponse getAllUsers(@RequestPayload GetAllUsersRequest request) {
        GetAllUsersResponse response = new GetAllUsersResponse();
        response.getUsers().addAll(userService.getAllUsers());

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "loginRequest")
    @ResponsePayload
    public LoginResponse login(@RequestPayload LoginRequest request) {
       LoginResponse response = new LoginResponse();
        response.setUser(userService.login("test", "test"));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "logoutRequest")
    @ResponsePayload
    public LogoutResponse logout(@RequestPayload LogoutRequest request) {
        LogoutResponse response = new LogoutResponse();

        if(UserService.session == null) {
            response.setLogoutMessage("You're not login !");
        } else {
            userService.logout();
            response.setLogoutMessage("Logout Successfully !");
        }

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllTrainsRequest")
    @ResponsePayload
    public GetAllTrainsResponse getAllTrains() {
        GetAllTrainsResponse response = new GetAllTrainsResponse();

        List<Train> allTrains = trainService.getAllTrain(null);
        response.getTrains().addAll(allTrains);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "filterAvailableTrainsRequest")
    @ResponsePayload
    public FilterAvailableTrainsResponse filterAvailableTrains(@RequestPayload FilterAvailableTrainsRequest request) {
        FilterAvailableTrainsResponse response = new FilterAvailableTrainsResponse();

        List<Train> allTrains = trainService.getAllTrain(request);
        response.getTrains().addAll(allTrains);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "createReservationRequest")
    @ResponsePayload
    public CreateReservationResponse createReservation(@RequestPayload CreateReservationRequest request) {
        CreateReservationResponse response = new CreateReservationResponse();

        if(UserService.session != null) {

            Reservation reservation = trainService.createReservation(request);

            if(reservation!=null) {

                reservation.setIdUser(UserService.session.getId());
                UserService.session.getReservations().add(reservation);

                response.setMessage("Reservation created successfully !");
            } else {
                response.setMessage("A problem occurred while processing your reservation, the ticket(s) maybe not available !");
            }
        }
        else {
            response.setMessage("You need to be login to create a reservation !");
        }

        return response;
    }
}
