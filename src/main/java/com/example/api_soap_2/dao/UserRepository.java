package com.example.api_soap_2.dao;

import fr.model.user.User;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserRepository {
    private static final List<User> users = new ArrayList<>();

    @PostConstruct
    public void initData() {
        User test = new User();

        test.setId(1);
        test.setUserName("test");
        test.setPassword("test");

        users.add(test);

    }

    public List<User> findAllUsers(){
        return users;
    }

}